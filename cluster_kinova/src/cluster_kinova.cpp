//clusterIros.cpp
//Author:Camilo Perez
//Date:Dic 13 2012
//Modified: March 4 2013
//Modified: June 10 2013

#include <pcl/filters/voxel_grid.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/crop_hull.h>
#include <pcl/segmentation/extract_clusters.h>
// input/output management
#include <iostream>
// file input/output management
#include <fstream>
#include <pcl/common/centroid.h>
#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/thread/thread.hpp>

#include <pcl/filters/passthrough.h>
#include <pcl/octree/octree.h>
#include <iostream>
#include <vector>
#include <ctime>

//include cluster message
#include "cluster_kinova/cluster.h"
#include <geometry_msgs/Point.h>
//#include "bodytracker/openniCursorPCL.h"

//#include <ipc_bridge/ipc_bridge.h>

#include <geometry_msgs/Twist.h>
//#include <ipc_bridge/msgs/geometry_msgs_Twist.h>
#define RESOLUTION       0.01  //Octree Resolution
#define PI               3.1416

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
int cloudLife = 0;
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_hull(new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ> cloud;
pcl::PointCloud<pcl::PointXYZ>::Ptr  cloud_projected(new pcl::PointCloud<pcl::PointXYZ>),
                                                     cloud_p(new pcl::PointCloud<pcl::PointXYZ>);
pcl::ModelCoefficients coeff_cube;
pcl::PointXYZ min_pt, max_pt;
//float coefficientsTest[3] = {1, 0, 0};
Eigen::Vector3f normalToPlane(1, 0, 0);

int objectNumber = 0;
pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);

struct tableObjects {
    pcl::PointXYZ top;
    pcl::PointXYZ bottom;
    pcl::PointXYZ centroidPoint;
} object[15], objectM[15];

int clusterSize = 0;

void callback(const PointCloud::ConstPtr& msg) {
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered1(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered2(new pcl::PointCloud<pcl::PointXYZ>);

    int nonanValues = 0;
    int index[50000];

    if(cloudLife == 0) {
        int i, g = 0;

        pcl::VoxelGrid<pcl::PointXYZ> sor;  //create downsampling filter
        sor.setInputCloud(msg);
        sor.setLeafSize(0.01f, 0.01f, 0.01f);
        sor.filter(*cloud_filtered2);

        pcl::PassThrough<pcl::PointXYZ> pass;
        pass.setInputCloud(cloud_filtered2);
        pass.setFilterFieldName("z");
        pass.setFilterLimits(0, 1.75);
        pass.setKeepOrganized( true );
        pass.filter(*cloud_filtered1);
        std::cerr << "PointCloud after filtering 1  has: " << cloud_filtered1->points.size() << " data points." << std::endl;

        pcl::PassThrough<pcl::PointXYZ> pass2;
        pass.setInputCloud(cloud_filtered1);
        pass.setFilterFieldName("x");
        pass.setFilterLimits(-0.4, 0.6);
        pass.filter(*cloud_filtered);
        std::cerr << "PointCloud after filtering 2 has: " << cloud_filtered->points.size() << " data points." << std::endl;
        std::cerr << "PointCloud after filtering: " << cloud_filtered->width * cloud_filtered->height << " data points." << std::endl;

        for(size_t n = 0; n < cloud_filtered->points.size(); ++n) {
            if(!(std::isnan(cloud_filtered->points[n].y))) {
                index[nonanValues] = n;
                nonanValues++;
            }
        }
        std::cerr << "The number of nonan values are: " << nonanValues << " " << std::endl;

        // Fill in the cloud data
        cloud.width  = nonanValues;
        cloud.height = 1;
        cloud.points.resize(cloud.width * cloud.height);

        // Generate the data
        for(size_t i = 0; i < cloud.points.size(); ++i) {
            cloud.points[i].x = cloud_filtered->points[index[i]].x;
            cloud.points[i].y = cloud_filtered->points[index[i]].y;
            cloud.points[i].z = cloud_filtered->points[index[i]].z;
        }

        pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
        // Create the segmentation object
        pcl::SACSegmentation<pcl::PointXYZ> seg;
        // Optional
        seg.setOptimizeCoefficients(true);
        // Mandatory
        seg.setModelType(pcl::SACMODEL_PLANE);
        seg.setMethodType(pcl::SAC_RANSAC);
        seg.setDistanceThreshold(0.05);

        seg.setInputCloud(cloud.makeShared());
        seg.segment(*inliers, *coefficients);
        normalToPlane[0] = coefficients->values[0];
        normalToPlane[1] = coefficients->values[1];
        normalToPlane[2] = coefficients->values[2];

        std::cerr << "Inliers size: " <<inliers->indices.size() << std::endl;

        if(inliers->indices.size() == 0) {
            PCL_ERROR("Could not estimate a planar model for the given dataset.");
        }

        // Project the model inliers
        pcl::ProjectInliers<pcl::PointXYZ> proj;
        proj.setModelType(pcl::SACMODEL_PLANE);
        proj.setIndices(inliers);
        proj.setInputCloud(cloud.makeShared());
        proj.setModelCoefficients(coefficients);
        proj.filter(*cloud_projected);
        std::cerr << "PointCloud after projection has: " << cloud_projected->points.size() << " data points." << std::endl;

        pcl:: ExtractPolygonalPrismData<pcl::PointXYZ> ex;
        ex.setInputCloud(cloud.makeShared());
        ex.setInputPlanarHull(cloud_projected);

        ex.setHeightLimits(0.02, 0.35);

        pcl::PointIndices::Ptr output(new pcl::PointIndices());
        ex.segment(*output);

        pcl::ExtractIndices<pcl::PointXYZ> extract;

        extract.setInputCloud(cloud.makeShared());
        extract.setIndices(output);
        extract.setNegative(false);
        extract.filter(*cloud_p);
        /////////////*******************************************

        /////////////////////EUCLIDEAN CLUSTER////////////////////////////////////////
        // Creating the KdTree object for the search method of the extraction
        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
        tree->setInputCloud(cloud_p);

        std::vector<pcl::PointIndices> cluster_indices;
        pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
        ec.setClusterTolerance(0.03); // 2cm
        ec.setMinClusterSize(40);
        ec.setMaxClusterSize(25000);
        ec.setSearchMethod(tree);
        ec.setInputCloud(cloud_p);
        ec.extract(cluster_indices);

        objectNumber = 0;

        for(std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it) {
            pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZ>);
            for(std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); pit++) {
                cloud_cluster->points.push_back(cloud_p->points[*pit]); //*
            }
            cloud_cluster->width = cloud_cluster->points.size();
            cloud_cluster->height = 1;
            cloud_cluster->is_dense = true;

            std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size() << " data points." << std::endl;

            pcl::getMinMax3D(cloud_cluster.operator*(), object[objectNumber].bottom, object[objectNumber].top);
            Eigen::Matrix< float, 4, 1 > centroid;


            pcl::compute3DCentroid(cloud_cluster.operator*(), centroid);
            object[objectNumber].centroidPoint.x = centroid[0];
            object[objectNumber].centroidPoint.y = centroid[1];
            object[objectNumber].centroidPoint.z = centroid[2];

            objectNumber++;
        }
        ROS_INFO("Number of clusters:%d", objectNumber);
        clusterSize = objectNumber;
        for(int h = 0; h < clusterSize; h++) {
            objectM[h] = object[h];
        }
    }

    cloudLife++;

    if(cloudLife == 5) {
        cloudLife = 0;
    }

    viewer->addPointCloud<pcl::PointXYZ>(cloud_p, "sample cloud2");
    std::string name = "cube";
    std::string sphereName = "sphere";
    std::string result;
    std::string sphereNameIndex;

    Eigen::Quaternionf quaternionBox;
    quaternionBox.x() = normalToPlane[0];
    quaternionBox.y() = normalToPlane[1];
    quaternionBox.z() = normalToPlane[2];
    quaternionBox.w() = 0;


    for(int  k = 0; k < objectNumber; k++) {
        result = name + boost::lexical_cast<std::string>(k);
        sphereNameIndex = sphereName + boost::lexical_cast<std::string>(k);
        viewer->addCube(object[k].bottom.x, object[k].top.x, object[k].bottom.y, object[k].top.y, object[k].bottom.z, object[k].top.z, 1.0, (double)rand()/RAND_MAX, (double)k/objectNumber, result);
        viewer->addSphere(object[k].centroidPoint, 0.025, 0.0, 1.0, 0.0, sphereNameIndex);
        std::cout << "Cluster:  " << result <<"bottom(x, y, z):("<<object[k].bottom.x <<", "<<object[k].bottom.y<<", "<<object[k].bottom.z<<")" << std::endl;
    }

    Eigen::Vector3f  translationBox;

    translationBox[0] = object[0].centroidPoint.x;
    translationBox[1] = object[0].centroidPoint.y;
    translationBox[2] = object[0].centroidPoint.z;

    viewer->addCube(translationBox, quaternionBox, object[0].top.x-object[0].bottom.x, object[0].top.y-object[0].bottom.y, object[0].top.z-object[0].bottom.z, "cube");
    viewer->addPlane( *coefficients, "plane", 0);

    //******Calculate circle parallel to support plane around centroid*************************//
    pcl::PointXYZ circlePoint[8];
    Eigen::Vector3f parallelToPlaneA;
    Eigen::Vector3f parallelToPlaneB;
    float radius = 0.3;
    parallelToPlaneA[0] = 1.0;
    parallelToPlaneA[1] = 1.0;
    parallelToPlaneA[2] = (-normalToPlane[0] - normalToPlane[1]) /normalToPlane[2];
    parallelToPlaneA.normalize();

    parallelToPlaneB = parallelToPlaneA.cross(normalToPlane);

    std::string circleSphereName = "circleSphere";
    std::string circleSphereNumber;

    /* //uncomment for sphere circle visualization
       for(int h = 0;h<8;h++)
       {
       circleSphereNumber = circleSphereName + boost::lexical_cast<std::string>(h);
       circlePoint[h].x = object[0].centroidPoint.x+radius*cos(PI/4*h)*parallelToPlaneA[0]+radius*sin(PI/4*h)*parallelToPlaneB[0];
       circlePoint[h].y = object[0].centroidPoint.y+radius*cos(PI/4*h)*parallelToPlaneA[1]+radius*sin(PI/4*h)*parallelToPlaneB[1];
       circlePoint[h].z = object[0].centroidPoint.z+radius*cos(PI/4*h)*parallelToPlaneA[2]+radius*sin(PI/4*h)*parallelToPlaneB[2];

       viewer->addSphere(circlePoint[h], 0.015, 1.0, 0.0, 0.0, circleSphereNumber);

       }
       */

    // viewer->addSphere(circlePoint[0], 0.025, 0.0, 1.0, 0.0, "circleSphere");

    //******END Calculate circle parallel to support plane around centroid*************************//

    //  viewer->addCube(min_pt.x, max_pt.x, min_pt.y, max_pt.y, min_pt.z, max_pt.z, 1.0, 0.0, 0.0, "cube");

    viewer->spinOnce(100);

    /*  //uncomment for sphere circle visualization
        for(int h = 0;h<8;h++)
        {
        circleSphereNumber = circleSphereName + boost::lexical_cast<std::string>(h);
        viewer->removeShape(circleSphereNumber);

        }
        */


    for(int  k = 0; k < objectNumber; k++) {
        result = name + boost::lexical_cast<std::string>(k);
        sphereNameIndex = sphereName + boost::lexical_cast<std::string>(k);
        viewer->removeShape(result);
        viewer->removeShape(sphereNameIndex);
    }
    viewer->removeShape("cube");
    viewer->removeShape("plane");
    viewer->removeShape("circleSphere");
    viewer->removePointCloud("sample cloud2");

}

int main(int argc, char** argv) {
    ros::init(argc, argv, "cluster_kinova");
    ros::NodeHandle nh;

    viewer->setBackgroundColor(0, 0, 0);
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
    viewer->addCoordinateSystem(1.0);
    viewer->initCameraParameters();

    //  ROS_INFO("Reading from kinect depth points");

    ros::Subscriber sub = nh.subscribe<PointCloud>("camera/depth_registered/points", 1, callback);
    ros::Publisher chatter_pub = nh.advertise<cluster_kinova::cluster>("cluster", 100);
    ros::Rate loop_rate(10);
    ROS_INFO("Main loop");

    while(ros::ok()) {
        cluster_kinova::cluster msg_cluster;

        msg_cluster.clusterSize = clusterSize;
        msg_cluster.normalToPlaneVector.x = normalToPlane[0];
        msg_cluster.normalToPlaneVector.y = normalToPlane[1];
        msg_cluster.normalToPlaneVector.z = normalToPlane[2];

        for(int j = 0; j<clusterSize; j++) {
            msg_cluster.centroid[j].x = objectM[j].centroidPoint.x;
            msg_cluster.centroid[j].y = objectM[j].centroidPoint.y;
            msg_cluster.centroid[j].z = objectM[j].centroidPoint.z;
            msg_cluster.top[j].x = objectM[j].top.x;
            msg_cluster.top[j].y = objectM[j].top.y;
            msg_cluster.top[j].z = objectM[j].top.z;
            msg_cluster.bottom[j].x = objectM[j].bottom.x;
            msg_cluster.bottom[j].y = objectM[j].bottom.y;
            msg_cluster.bottom[j].z = objectM[j].bottom.z;
        }
        chatter_pub.publish(msg_cluster);
        ros::spinOnce();

        loop_rate.sleep();
    }
}
