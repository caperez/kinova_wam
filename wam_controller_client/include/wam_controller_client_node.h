#include <ros/ros.h>
//#include <actionlib/client/simple_action_client.h>
//#include <actionlib/client/terminal_state.h>
//#include <jaco_msgs/ArmPoseAction.h>
#include <iostream>
#include <Eigen/Geometry>
#include "std_srvs/Empty.h"
#include "pcl_interface_kinova/selectedObject.h"
#include <geometry_msgs/Pose.h>
#include "wam_controller_client/GraspOrientation.h"
#include <boost/thread.hpp>
#include "wam_planning/IKPose.h"
#define TOP 1
#define LEFT 2
#define RIGHT 3
#define ROTATE 4
#define PUSH_PULL 5

#define JOYSTICK 1
#define AUTOPICK 2
#define PICK 3
#define PLACE 4

class WamControllerClientNode {

  private:

    struct circularSpheres {
        float point[3];
        Eigen::Quaternionf orientation;
    }circularSphere[8];

    int mode_state;
    int cursor_state;
    float top_sphere_location[3];
    Eigen::Quaternionf top_sphere_orientation;
    bool performing_trajectory;
    bool trajectory_updated;
    ros::NodeHandle node;

    ros::ServiceServer wam_go_grasp_server;
    ros::Subscriber poses_sub;
    geometry_msgs::Pose target_pose;

    //Service Client
    ros::ServiceClient moveit_pose_client;
    // Topic subscription callbacks
    void poses_callback(const pcl_interface_kinova::selectedObject::ConstPtr& selected_object);

    // Service Servers callbacks
    bool wam_go_grasp(wam_controller_client::GraspOrientation::Request &goal,wam_controller_client::GraspOrientation::Response &b);
    void update_simulation_target(int grasp_direction);



  public:
    WamControllerClientNode();
};
