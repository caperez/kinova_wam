//Author:Camilo Perez
//Date:August 18 2012
//Modified on: June 13 2013
// input/output management
#include <iostream>
// file input/output management
#include <fstream>
#include <math.h>
#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/distances.h>
#include <boost/foreach.hpp>
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/thread/thread.hpp>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/passthrough.h>
#include "visual_interface_kinova/CamshiftBox.h"
#include "visual_interface_kinova/centroidPixelLocation.h"
#include <pcl/octree/octree.h>
#include <iostream>
#include <vector>
#include <ctime>
#include "pcl_interface_kinova/targetPoints.h"
#include "pcl_interface_kinova/selectedObject.h"
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
//Include Cluster lib
#include "cluster_kinova/cluster.h"
//Include GUI states
#include "visual_interface_kinova/guiButtonState.h"
#include <Eigen/Geometry>

#define RESOLUTION       0.02   //Octree Resolution
#define PI               3.14159265359

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));

pcl::PointXYZ targetPoint;
pcl::PointXYZ topGraspPoint;
pcl::PointXYZ trackingPoint;
pcl::PointXYZ leftSphere;
pcl::PointXYZ rightSphere;



float distanceCentroidHitPoint = 0.0;

float top_grasp_distance = 0.3;
//float grasp_radius = 0.2;
float grasp_delta = 0.17;
float top_delta =0.15;



float top_place_delta=0.35;
float top_pick_delta=0.25;

//right and left deltas for pick and place
float left_delta=0.2;
float right_delta=0.2;
float up_delta=0.25;
float front_delta=0.2;
int circleSphereDivision = 8;

int centroidU[15] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int centroidV[15] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int numberOfObjects = 0;

float u = 100, v = 100, z = 0, uTarget = 100, vTarget = 100;   //depth of u and v coordinate
//modeState, 0:JOYSTICK, 1:AUTO-PICK, 2:PICK, 3:PLACE
//Cursor clicking modes. none:0, top circle:1, leftCircle:2, rightCircle = 3
int modeState = 0, cursorState = 0;

Eigen::Matrix4f transformationMatrix;
Eigen::Vector4f topSphereKinect, topSphereWam, centroidSphere;
Eigen::Vector4f sphereKinect, leftSphereKinect, sphereWam,leftSphereWam,rightSphereKinect,rightSphereWam;
//Make sure the size of this array is equal to circleSphereDivision
struct circularSpheres {
    float point[3];
    Eigen::Quaternionf orientation;
} circularSphere[8];

struct tableObjects {
    pcl::PointXYZ top;
    pcl::PointXYZ bottom;
    pcl::PointXYZ centroidPoint;
} objects[15];

Eigen::Vector3f normalToPlane(1, 0, 0);

pcl::PointXYZ topCentroid[15];
int clusterSizeSend = 0;
int clusterSize = 0;

// Parameters for this will become part of the message the visual interface gives us
Eigen::Quaternionf get_orientation_from_scene(float *m_point,
                                              float *m_centroid,
                                              float *m_plane_normal) {
    //Force normal to be in z direction of the WAM arm
    //Assumes grasping objects are over a parallel plane to the floor
    //For using normal to the detected plane comment the next three lines
    m_plane_normal[0] = 0;
    m_plane_normal[1] = 0;
    m_plane_normal[2] = 1;

    //If using normal from the detected plane, force normal to be positive
    if(m_plane_normal[2]<0) {
        m_plane_normal[0] *= -1;
        m_plane_normal[1] *= -1;
        m_plane_normal[2] *= -1;
    }

    if(m_point[1] > m_centroid[1]) {
        m_plane_normal[0] *= -1;
        m_plane_normal[1] *= -1;
        m_plane_normal[2] *= -1;
    }

    Eigen::Vector3f point(m_point);
    Eigen::Vector3f centroid(m_centroid);
    Eigen::Vector3f z1 = centroid - point;
    Eigen::Vector3f y1(m_plane_normal);
    y1.normalize();
    z1.normalize();
    Eigen::Vector3f x1 = y1.cross(z1);
    x1.normalize();
    std::cout << "x1:" << std::endl << x1 << std::endl;
    std::cout << "y1:" << std::endl << y1 << std::endl;
    std::cout << "z1:" << std::endl << z1 << std::endl;
    std::cout << std::endl;

    Eigen::Vector3f x(1, 0, 0);
    Eigen::Vector3f y(0, 1, 0);
    Eigen::Vector3f z(0, 0, 1);

    Eigen::Matrix3f rotationMatrix;

    rotationMatrix(0, 0) = x1.dot(x);
    rotationMatrix(1, 0) = x1.dot(y);
    rotationMatrix(2, 0) = x1.dot(z);

    rotationMatrix(0, 1) = y1.dot(x);
    rotationMatrix(1, 1) = y1.dot(y);
    rotationMatrix(2, 1) = y1.dot(z);

    rotationMatrix(0, 2) = z1.dot(x);
    rotationMatrix(1, 2) = z1.dot(y);
    rotationMatrix(2, 2) = z1.dot(z);

    std::cout << "Rotation Matrix: " << std::endl;
    std::cout << rotationMatrix << std::endl;
    std::cout << std::endl;

    Eigen::Quaternionf rotation_quaternion(rotationMatrix);
    rotation_quaternion.normalize();
    return rotation_quaternion;
}

//Getting states from GUI buttons and cursor
void guiCallback(const visual_interface_kinova::guiButtonState::ConstPtr& msg4) {
    modeState = msg4->modeState;
    cursorState = msg4->cursorState;
}

Eigen::Matrix4f read_transform_matrix() {
    std::ifstream transform_file;
    std::string kinova_workspace = getenv("CALIBRATION_WORKSPACE");
    std::string transform_file_path = kinova_workspace + "/kinect_wam_transform.txt";
    transform_file.open(transform_file_path.c_str(), std::ios_base::in | std::ios_base::binary);

    if(!transform_file) {
        std::cerr << "Can't open transform file" << std::endl;
        std::exit(-1);
    }

    std::string line;
    Eigen::Matrix4f transform_matrix;
    int i = 0;
    while(getline(transform_file, line) && i < 4) {
        std::istringstream in(line);
        float c1, c2, c3, c4;
        in >> c1 >> c2 >> c3 >> c4;

        transform_matrix(i, 0) = c1;
        transform_matrix(i, 1) = c2;
        transform_matrix(i, 2) = c3;
        transform_matrix(i, 3) = c4;
        ++i;
    }

    std::cout << transform_matrix <<std::endl;
    return transform_matrix;
    //    return transform_matrix.inverse();
}

//Getting objects bounding box, centroid and vector Normal to Plane
void clusterCallback(const cluster_kinova::cluster::ConstPtr& msg3) {

    clusterSize = msg3->clusterSize;
    normalToPlane[0] = msg3->normalToPlaneVector.x;
    normalToPlane[1] = msg3->normalToPlaneVector.y;
    normalToPlane[2] = msg3->normalToPlaneVector.z;
    for(int j = 0; j<clusterSize; j++) {
        objects[j].centroidPoint.x = msg3->centroid[j].x;
        objects[j].centroidPoint.y = msg3->centroid[j].y;
        objects[j].centroidPoint.z = msg3->centroid[j].z;

        objects[j].top.x = msg3->top[j].x;
        objects[j].top.y = msg3->top[j].y;
        objects[j].top.z = msg3->top[j].z;

        objects[j].bottom.x = msg3->bottom[j].x;
        objects[j].bottom.y = msg3->bottom[j].y;
        objects[j].bottom.z = msg3->bottom[j].z;
    }
}

void chatterCallback(const visual_interface_kinova::CamshiftBox::ConstPtr& msg2) {
  u = msg2->x;
  v = msg2->y;
  uTarget = msg2->targetx;
  vTarget = msg2->targety;
}

void callback(const PointCloud::ConstPtr& msg) {
    int i;
    int iterator;
    int tam;
    float hitObjectDistance = 0.0, hitObjectDistanceTemp = 0.0;
    pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(RESOLUTION);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);

    pcl::PointXYZ searchPoint;

    /////////////////////////////////
    // Create the filtering object
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud(msg);
    pass.setFilterFieldName("x");
    pass.setFilterLimits(-1.5, 0.9);
    //pass.setFilterLimitsNegative(true);
    pass.setKeepOrganized( true );
    pass.filter(*cloud_filtered);

    trackingPoint = cloud_filtered->points[(int)v * cloud_filtered->width + (int)u];
    targetPoint = cloud_filtered->points[(int)vTarget * cloud_filtered->width + (int)uTarget];

    if(modeState == 1 || modeState== 4) {
        //Find the closest pixel point to the objects centroid
        pcl::KdTree<pcl::PointXYZ>::Ptr tree_(new pcl::KdTreeFLANN<pcl::PointXYZ>);
        tree_->setInputCloud(cloud_filtered);
        std::vector<int> nn_indices(1);
        std::vector<float> nn_dists(1);

        for(int i = 0;i<clusterSize;i++) {
            tree_->nearestKSearch( objects[i].centroidPoint, 1, nn_indices, nn_dists);

            //    ROS_INFO("The index of object(%d) is: %d", i, nn_indices[0]);
            centroidV[i] = nn_indices[0] / 640;
            // ROS_INFO("The V pixel location is: %d", centroidV[i]);
            centroidU[i] = nn_indices[0] % 640;
            // ROS_INFO("The U pixel location is: %d", centroidU[i]);
        }

        numberOfObjects = clusterSize;

        //printf("The closest point of (0, 0, 0) is: (%f, %f, %f)", cloud_filtered->points[nn_indices[0]].x, cloud_filtered->points[nn_indices[0]].y, cloud_filtered->points[nn_indices[0]].z);
        //END Find the closest pixel point to the objects centroid

        //Find the nearest centroid after clicking/////////////////////////////

        //This section corrects the hit point
        topGraspPoint.x = top_grasp_distance*normalToPlane[0] + objects[0].centroidPoint.x;
        topGraspPoint.y = top_grasp_distance*normalToPlane[1] + objects[0].centroidPoint.y;
        topGraspPoint.z = top_grasp_distance*normalToPlane[2] + objects[0].centroidPoint.z;

        //topCentroid[0] = targetPoint;
        int winner = 0;
        if(clusterSize > 0) {
            hitObjectDistance = pcl::euclideanDistance(targetPoint, objects[0].centroidPoint);
            // topGraspPoint = objects[0].centroidPoint;
            //Find the grap sphere normal to the plane and above the select object
            topGraspPoint.x = top_grasp_distance*normalToPlane[0] + objects[0].centroidPoint.x;
            topGraspPoint.y = top_grasp_distance*normalToPlane[1] + objects[0].centroidPoint.y;
            topGraspPoint.z = top_grasp_distance*normalToPlane[2] + objects[0].centroidPoint.z;


            //END Find the grap sphere normal to the plane and above the select object

            //searching in all the cluster for the nearest object to the hit point
            for(int i = 1; i < clusterSize; ++i) {
                hitObjectDistanceTemp= pcl::euclideanDistance(targetPoint, objects[i].centroidPoint);
                if(hitObjectDistanceTemp < hitObjectDistance) {
                    topGraspPoint.x = top_grasp_distance*normalToPlane[0] + objects[i].centroidPoint.x;
                    topGraspPoint.y = top_grasp_distance*normalToPlane[1] + objects[i].centroidPoint.y;
                    topGraspPoint.z = top_grasp_distance*normalToPlane[2] + objects[i].centroidPoint.z;
                    hitObjectDistance = hitObjectDistanceTemp;
                    winner = i;
                }
            }
	    top_grasp_distance=sqrt(pow((objects[winner].top.y-objects[winner].bottom.y),2)+pow((objects[winner].top.z-objects[winner].bottom.z),2))/2+top_delta;

	    topGraspPoint.x = top_grasp_distance*normalToPlane[0] + objects[winner].centroidPoint.x;
	    topGraspPoint.y = top_grasp_distance*normalToPlane[1] + objects[winner].centroidPoint.y;
	    topGraspPoint.z = top_grasp_distance*normalToPlane[2] + objects[winner].centroidPoint.z;





            distanceCentroidHitPoint = hitObjectDistance;
	    // clusterSizeSend = clusterSize;
	    // int f = 1;
	    /*    for(int g = 0; g < clusterSizeSend; g++) {
                if(g == winner) {
                    topCentroid[0].x = top_grasp_distance*normalToPlane[0] + objects[g].centroidPoint.x;
                    topCentroid[0].y = top_grasp_distance*normalToPlane[1] + objects[g].centroidPoint.y;
                    topCentroid[0].z = top_grasp_distance*normalToPlane[2] + objects[g].centroidPoint.z;
                }
                else {
                    topCentroid[f].x = top_grasp_distance*normalToPlane[0] + objects[g].centroidPoint.x;
                    topCentroid[f].y = top_grasp_distance*normalToPlane[1] + objects[g].centroidPoint.y;
                    topCentroid[f].z = top_grasp_distance*normalToPlane[2] + objects[g].centroidPoint.z;
                    f++;
                }
            }*/
            ROS_INFO("BLUE SPHERE COORDINATES(X, Y, Z):(%f, %f, %f)", topGraspPoint.x, topGraspPoint.y, topGraspPoint.z);
        }
        //End correction

        //END Find the nearest centroid after clicking/////////////////////////////
        //******Calculate circle parallel to support plane around centroid*************************//
        pcl::PointXYZ circlePoint[8];
        Eigen::Vector3f parallelToPlaneA;
        Eigen::Vector3f parallelToPlaneB;

        Eigen::Vector4f circleSphereKinect;
        Eigen::Vector4f centroidKinect;
        Eigen::Vector4f topNormalToPlaneKinect;
        Eigen::Vector4f circleSphereWam;
        Eigen::Vector4f centroidWam;
        Eigen::Vector4f normalToPlaneWam;
        Eigen::Vector4f zeroVector(0, 0, 0, 0);
        Eigen::Quaternionf toolOrientation;
        float opPoint[3] = {0, 0, 0};
        float opCentroid[3] = {0, 0, 0};
        float opPlaneNormal[3] = {0, 0, 0};

        parallelToPlaneA[0] = 1.0;
        parallelToPlaneA[1] = 1.0;
        parallelToPlaneA[2] = (-normalToPlane[0] - normalToPlane[1]) / normalToPlane[2];
        parallelToPlaneA.normalize();

        parallelToPlaneB = parallelToPlaneA.cross(normalToPlane);

        std::string circleSphereName = "circleSphere";
        std::string circleSphereNumber;

        centroidKinect[0] = objects[winner].centroidPoint.x;
        centroidKinect[1] = objects[winner].centroidPoint.y;
        centroidKinect[2] = objects[winner].centroidPoint.z;
        centroidKinect[3] = 1;
        //get centroid select object in WAM reference frame
        centroidWam = transformationMatrix*centroidKinect;
        opCentroid[0] = centroidWam[0];
        opCentroid[1] = centroidWam[1];
        opCentroid[2] = centroidWam[2];

        centroidSphere[0] = opCentroid[0];
        centroidSphere[1] = opCentroid[1];
        centroidSphere[2] = opCentroid[2];

        topNormalToPlaneKinect[0] = normalToPlane[0];
        topNormalToPlaneKinect[1] = normalToPlane[1];
        topNormalToPlaneKinect[2] = normalToPlane[2];
        topNormalToPlaneKinect[3] = 1;
        //tansform normal to the plane kinect frame to WAM frame
        normalToPlaneWam = transformationMatrix * topNormalToPlaneKinect - transformationMatrix * zeroVector;

        opPlaneNormal[0] = normalToPlaneWam[0];
        opPlaneNormal[1] = normalToPlaneWam[1];
        opPlaneNormal[2] = normalToPlaneWam[2];

        for(int h = 0;h<circleSphereDivision;h++) {
            circleSphereNumber = circleSphereName + boost::lexical_cast<std::string>(h);
            circlePoint[h].x = objects[winner].centroidPoint.x+((objects[winner].top.x-objects[winner].bottom.x)/2+grasp_delta)*cos(2*PI/circleSphereDivision*h)*parallelToPlaneA[0]+((objects[winner].top.x-objects[winner].bottom.x)/2+grasp_delta)*sin(2*PI/circleSphereDivision*h)*parallelToPlaneB[0];
            circlePoint[h].y = objects[winner].centroidPoint.y+((objects[winner].top.x-objects[winner].bottom.x)/2+grasp_delta)*cos(2*PI/circleSphereDivision*h)*parallelToPlaneA[1]+((objects[winner].top.x-objects[winner].bottom.x)/2+grasp_delta)*sin(2*PI/circleSphereDivision*h)*parallelToPlaneB[1];
            circlePoint[h].z = objects[winner].centroidPoint.z+((objects[winner].top.x-objects[winner].bottom.x)/2+grasp_delta)*cos(2*PI/circleSphereDivision*h)*parallelToPlaneA[2]+((objects[winner].top.x-objects[winner].bottom.x)/2+grasp_delta)*sin(2*PI/circleSphereDivision*h)*parallelToPlaneB[2];

            circleSphereKinect[0] = circlePoint[h].x;
            circleSphereKinect[1] = circlePoint[h].y;
            circleSphereKinect[2] = circlePoint[h].z;
            circleSphereKinect[3] = 1;
            //calculate circle spheres centroid with respect to the WAM
            circleSphereWam = transformationMatrix * circleSphereKinect;
            opPoint[0] = circleSphereWam[0];
            opPoint[1] = circleSphereWam[1];
            opPoint[2] = circleSphereWam[2];

            toolOrientation= get_orientation_from_scene(opPoint, opCentroid, opPlaneNormal);

            circularSphere[h].point[0] = opPoint[0];
            circularSphere[h].point[1] = opPoint[1];
            circularSphere[h].point[2] = opPoint[2];

            circularSphere[h].orientation.x() = toolOrientation.x();
            circularSphere[h].orientation.y() = toolOrientation.y();
            circularSphere[h].orientation.z() = toolOrientation.z();
            circularSphere[h].orientation.w() = toolOrientation.w();

            viewer->addSphere(circlePoint[h], 0.015, 0.54, 0.0, 0.54, circleSphereNumber);

        }
        //******END Calculate circle parallel to support plane around centroid*************************//

        viewer->addPointCloud<pcl::PointXYZ>(cloud_filtered, "sample cloud2");
        viewer->addSphere(targetPoint, 0.025, 1.0, 0.0, 0.0, "targetPoint");
        viewer->addSphere(topGraspPoint, 0.025, 0.0, 0.0, 1.0, "topGraspPointSphere");
        topSphereKinect[0] = topGraspPoint.x;
        topSphereKinect[1] = topGraspPoint.y;
        topSphereKinect[2] = topGraspPoint.z;
        topSphereKinect[3] = 1;

        topSphereWam = transformationMatrix * topSphereKinect;

        ///*****Add Centroids to visualization*****//////////
        //std::string name = "cube";
        std::string sphereName = "sphere";
        std::string result;
        std::string sphereNameIndex;

        for(int  k = 0; k < clusterSize; k++) {
            sphereNameIndex = sphereName +boost::lexical_cast<std::string>(k);
            viewer->addSphere(objects[k].centroidPoint, 0.025, 0.0, 1.0, 0.0, sphereNameIndex);
        }
        //****END Add Centroids to visualization*****//////////

        viewer->spinOnce(100);

        viewer->removePointCloud("sample cloud2");
        viewer->removeShape("targetPoint");
        viewer->removeShape("topGraspPointSphere");

        //remove circle spheres
        for(int h = 0;h<circleSphereDivision;h++) {
            circleSphereNumber = circleSphereName + boost::lexical_cast<std::string>(h);
            viewer->removeShape(circleSphereNumber);
        }
        ///****Remove Centroids from Visualization**********////////////

        for(int  k = 0; k < clusterSize; k++) {
            sphereNameIndex = sphereName + boost::lexical_cast<std::string>(k);
            viewer->removeShape(sphereNameIndex);
        }
        ///****END Remove Centroids from Visualization**********////////////
    }
    else if(modeState==2 || modeState==3 || modeState==5)
      {

	float opPoint[3] = {0, 0, 0};
        float opCentroid[3] = {0, 0, 0};
        float opPlaneNormal[3] = {0, 0, 0};
	Eigen::Quaternionf toolOrientation;

	sphereKinect[0]=targetPoint.x;
	sphereKinect[1]=targetPoint.y;
	sphereKinect[2]=targetPoint.z;
	sphereKinect[3]=1;

	sphereWam = transformationMatrix * sphereKinect;

	//For Side Spheres calculation
	opCentroid[0]=sphereWam[0];
	opCentroid[1]=sphereWam[1];
	opCentroid[2]=sphereWam[2];

	opPlaneNormal[0]=0;
	opPlaneNormal[1]=0;
	opPlaneNormal[2]=1;


	//For TopSphere calculation
        topSphereKinect[0] = targetPoint.x;
        topSphereKinect[1] = targetPoint.y;
        topSphereKinect[2] = targetPoint.z;
        topSphereKinect[3] = 1;

        topSphereWam = transformationMatrix * topSphereKinect;

	//PICK MODE
	if(modeState==2)
	  {
	    opPoint[0]=topSphereWam[0];
	    opPoint[1]=topSphereWam[1]+left_delta;//left side sphere
	    opPoint[2]=topSphereWam[2];
	    toolOrientation= get_orientation_from_scene(opPoint, opCentroid, opPlaneNormal);

	    leftSphereWam[0]=opPoint[0];
	    leftSphereWam[1]=opPoint[1];
	    leftSphereWam[2]=opPoint[2];
	    leftSphereWam[3]=1;
	    leftSphereKinect=transformationMatrix.inverse()*leftSphereWam;
	    leftSphere.x=leftSphereKinect[0];
	    leftSphere.y=leftSphereKinect[1];
	    leftSphere.z=leftSphereKinect[2];



	    for(int h = 0;h<(circleSphereDivision/2);h++)
	      {
	      circularSphere[h].point[0] = opPoint[0];
	      circularSphere[h].point[1] = opPoint[1];
	      circularSphere[h].point[2] = opPoint[2];

	      circularSphere[h].orientation.x() = toolOrientation.x();
	      circularSphere[h].orientation.y() = toolOrientation.y();
	      circularSphere[h].orientation.z() = toolOrientation.z();
	      circularSphere[h].orientation.w() = toolOrientation.w();




	      }

	    opPoint[1]=topSphereWam[1]-right_delta;//right side sphere



	    rightSphereWam[0]=opPoint[0];
	    rightSphereWam[1]=opPoint[1];
	    rightSphereWam[2]=opPoint[2];
	    rightSphereWam[3]=1;
	    rightSphereKinect=transformationMatrix.inverse()*rightSphereWam;
	    rightSphere.x=rightSphereKinect[0];
	    rightSphere.y=rightSphereKinect[1];
	    rightSphere.z=rightSphereKinect[2];





	    toolOrientation= get_orientation_from_scene(opPoint, opCentroid, opPlaneNormal);

	    for(int h = (circleSphereDivision/2);h<circleSphereDivision;h++)
	      {
	      circularSphere[h].point[0] = opPoint[0];
	      circularSphere[h].point[1] = opPoint[1];
	      circularSphere[h].point[2] = opPoint[2];

	      circularSphere[h].orientation.x() = toolOrientation.x();
	      circularSphere[h].orientation.y() = toolOrientation.y();
	      circularSphere[h].orientation.z() = toolOrientation.z();
	      circularSphere[h].orientation.w() = toolOrientation.w();
	      }


	    topSphereWam[2] =topSphereWam[2]+top_pick_delta;


	  }


	//PULL & PUSH MODE
	if(modeState==5)
	  {
	    opPoint[0]=topSphereWam[0]-front_delta;
	    opPoint[1]=topSphereWam[1];
	    opPoint[2]=topSphereWam[2];
	    toolOrientation= get_orientation_from_scene(opPoint, opCentroid, opPlaneNormal);

	    leftSphereWam[0]=opPoint[0];
	    leftSphereWam[1]=opPoint[1];
	    leftSphereWam[2]=opPoint[2];
	    leftSphereWam[3]=1;
	    leftSphereKinect=transformationMatrix.inverse()*leftSphereWam;
	    leftSphere.x=leftSphereKinect[0];
	    leftSphere.y=leftSphereKinect[1];
	    leftSphere.z=leftSphereKinect[2];



	    for(int h = 0;h<(circleSphereDivision/2);h++)
	      {
	      circularSphere[h].point[0] = opPoint[0];
	      circularSphere[h].point[1] = opPoint[1];
	      circularSphere[h].point[2] = opPoint[2];

	      circularSphere[h].orientation.x() = toolOrientation.x();
	      circularSphere[h].orientation.y() = toolOrientation.y();
	      circularSphere[h].orientation.z() = toolOrientation.z();
	      circularSphere[h].orientation.w() = toolOrientation.w();




	      }
	    
	    //opPoint[1]=topSphereWam[1]-right_delta;//right side sphere



	    rightSphereWam[0]=topSphereWam[0];
	    rightSphereWam[1]=topSphereWam[1]-right_delta;
	    rightSphereWam[2]=topSphereWam[2];
	    rightSphereWam[3]=1;
	    rightSphereKinect=transformationMatrix.inverse()*rightSphereWam;
	    rightSphere.x=rightSphereKinect[0];
	    rightSphere.y=rightSphereKinect[1];
	    rightSphere.z=rightSphereKinect[2];





	    toolOrientation= get_orientation_from_scene(opPoint, opCentroid, opPlaneNormal);

	    for(int h = (circleSphereDivision/2);h<circleSphereDivision;h++)
	      {
	      circularSphere[h].point[0] = opPoint[0];
	      circularSphere[h].point[1] = opPoint[1];
	      circularSphere[h].point[2] = opPoint[2];

	      circularSphere[h].orientation.x() = toolOrientation.x();
	      circularSphere[h].orientation.y() = toolOrientation.y();
	      circularSphere[h].orientation.z() = toolOrientation.z();
	      circularSphere[h].orientation.w() = toolOrientation.w();
	      }


	    topSphereWam[2] =topSphereWam[2] + top_pick_delta;


	  }

	//PLACE MODE
	else if(modeState==3)
	  {
	    opPoint[0]=topSphereWam[0];
	    opPoint[1]=topSphereWam[1]+left_delta;//left side sphere
	    opPoint[2]=topSphereWam[2]+up_delta;
	    opCentroid[2]=opCentroid[2]+up_delta;
	    toolOrientation= get_orientation_from_scene(opPoint, opCentroid, opPlaneNormal);

	    leftSphereWam[0]=opPoint[0];
	    leftSphereWam[1]=opPoint[1];
	    leftSphereWam[2]=opPoint[2];
	    leftSphereWam[3]=1;
	    leftSphereKinect=transformationMatrix.inverse()*leftSphereWam;
	    leftSphere.x=leftSphereKinect[0];
	    leftSphere.y=leftSphereKinect[1];
	    leftSphere.z=leftSphereKinect[2];


	    for(int h = 0;h<(circleSphereDivision/2);h++)
	      {
	      circularSphere[h].point[0] = opPoint[0];
	      circularSphere[h].point[1] = opPoint[1];
	      circularSphere[h].point[2] = opPoint[2];

	      circularSphere[h].orientation.x() = toolOrientation.x();
	      circularSphere[h].orientation.y() = toolOrientation.y();
	      circularSphere[h].orientation.z() = toolOrientation.z();
	      circularSphere[h].orientation.w() = toolOrientation.w();
	      }


	    opPoint[1]=topSphereWam[1]-right_delta;//right side sphere
	    toolOrientation= get_orientation_from_scene(opPoint, opCentroid, opPlaneNormal);

	    rightSphereWam[0]=opPoint[0];
	    rightSphereWam[1]=opPoint[1];
	    rightSphereWam[2]=opPoint[2];
	    rightSphereWam[3]=1;
	    rightSphereKinect=transformationMatrix.inverse()*rightSphereWam;
	    rightSphere.x=rightSphereKinect[0];
	    rightSphere.y=rightSphereKinect[1];
	    rightSphere.z=rightSphereKinect[2];

	    for(int h = (circleSphereDivision/2);h<circleSphereDivision;h++)
	      {
	      circularSphere[h].point[0] = opPoint[0];
	      circularSphere[h].point[1] = opPoint[1];
	      circularSphere[h].point[2] = opPoint[2];

	      circularSphere[h].orientation.x() = toolOrientation.x();
	      circularSphere[h].orientation.y() = toolOrientation.y();
	      circularSphere[h].orientation.z() = toolOrientation.z();
	      circularSphere[h].orientation.w() = toolOrientation.w();
	      }

	    topSphereWam[2] =topSphereWam[2]+top_place_delta;
	  }

	topSphereKinect=transformationMatrix.inverse()*topSphereWam;
	topGraspPoint.x=topSphereKinect[0];
	topGraspPoint.y=topSphereKinect[1];
	topGraspPoint.z=topSphereKinect[2];


	//Temporal fix to visualize normal, this has to be change to calculate normal

	std::basic_string<char> name = "arrow";
	std::basic_string<char> name2 = "arrow2";

	viewer->addPointCloud<pcl::PointXYZ>(cloud_filtered, "sample cloud2");
        viewer->addSphere(targetPoint, 0.025, 1.0, 0.0, 0.0, "targetPoint");
        viewer->addSphere(topGraspPoint, 0.025, 0.0, 0.0, 1.0, "topGraspPointSphere");
	viewer->addSphere(leftSphere, 0.025, 0.0, 1.0, 0.0, "leftSphere");
	viewer->addSphere(rightSphere, 0.025, 0.0, 1.0, 0.0, "rightSphere");
	if(modeState==5)
	  {
	    viewer->removeShape("rightSphere");
	    if(cursorState==4) //rotate
	      {
		 viewer->addArrow<pcl::PointXYZ>(topGraspPoint,targetPoint,1.0,0.0,0.0,false,name2);
	      }
	    else
	      {
		viewer->addArrow<pcl::PointXYZ>(leftSphere,targetPoint,1.0,0.0,0.0,false,name);
	      }
	   
	   
	    
	  }

        viewer->spinOnce(100);

        viewer->removePointCloud("sample cloud2");
        viewer->removeShape("targetPoint");
        viewer->removeShape("topGraspPointSphere");
	viewer->removeShape("leftSphere");
	viewer->removeShape("rightSphere");
	if(modeState==5)
	  {

	    if(cursorState==4) //rotate
	      {
		viewer->removeShape("arrow2");
	   
	      }
	    else
	      {
		viewer->removeShape("arrow");
	      }
	   
       	  }


      }

    else {
        viewer->addPointCloud<pcl::PointXYZ>(cloud_filtered, "sample cloud2");
        viewer->addSphere(targetPoint, 0.025, 1.0, 0.0, 0.0, "targetPoint");
        viewer->spinOnce(100);
        viewer->removePointCloud("sample cloud2");
        viewer->removeShape("targetPoint");



    }
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "pcl_interface_kinova");
    ros::NodeHandle nh;

    ros::Subscriber sub2 = nh.subscribe("roi_camshift_planar_robot", 100, chatterCallback);
    ros::Subscriber sub3 = nh.subscribe("cluster", 100, clusterCallback);
    ros::Subscriber sub4 = nh.subscribe("guiButtonState", 10, guiCallback);
    ros::Publisher chatter_pub3 = nh.advertise<pcl_interface_kinova::targetPoints>("targetPoints", 10);
    ros::Publisher chatter_pub4 = nh.advertise<geometry_msgs::Twist>("cursor", 100);
    ros::Publisher chatter_pub5 = nh.advertise<visual_interface_kinova::centroidPixelLocation>("centroidPixelLocation", 10);
    ros::Publisher chatter_pub6 = nh.advertise<pcl_interface_kinova::selectedObject>("selectedObject", 10);

    viewer->setBackgroundColor(0, 0, 0);

    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
    viewer->addCoordinateSystem(1.0);
    viewer->initCameraParameters();

    ros::Subscriber sub = nh.subscribe<PointCloud>("camera/depth_registered/points", 1, callback);

    ros::Rate loop_rate(10);
    ROS_INFO("Main loop");

    transformationMatrix = read_transform_matrix();

    std::cout << "Quaternion: " << std::endl;
    std::cout << "[" << transformationMatrix(0, 0) <<"," << transformationMatrix(0, 1) << "," << transformationMatrix(0, 2) << "," << ";" << std::endl;
    std::cout << transformationMatrix(1, 0) << "," << transformationMatrix(1, 1) << "," << transformationMatrix(1, 2) <<"," << ";" << std::endl;
    std::cout << transformationMatrix(2, 0) << "," << transformationMatrix(2, 1) << "," << transformationMatrix(2, 2) <<"," << ";" << std::endl;
    std::cout << transformationMatrix(3, 0) << "," << transformationMatrix(3, 1) << "," << transformationMatrix(3, 2) <<"," << ";" << std::endl;

    while(ros::ok()) {
        geometry_msgs::Twist msg_cursor;
        pcl_interface_kinova::selectedObject msg_selectedObject;
        pcl_interface_kinova::targetPoints msg_targetPoints;
        visual_interface_kinova::centroidPixelLocation msg_pixels;
        ROS_INFO("u:%f \n", u);
        ROS_INFO("u:%f \n", v);
        ROS_INFO("uTarget:%f \n", uTarget);
        ROS_INFO("vTarget:%f \n", vTarget);

        ROS_INFO("Tracking Point(x, y, z):(%f, %f, %f) \n", trackingPoint.x, trackingPoint.y, trackingPoint.z);
        ROS_INFO("Target Point(x, y, z):(%f, %f, %f) \n", targetPoint.x, targetPoint.y, targetPoint.z);

        for(int i = 0;i<15;i++) {
            msg_pixels.u[i] = centroidU[i];
            msg_pixels.v[i] = centroidV[i];

        }
        msg_pixels.numberOfObjects = numberOfObjects;
        chatter_pub5.publish(msg_pixels);

        if(pcl::isFinite(targetPoint) || pcl::isFinite(trackingPoint)) {
            if(pcl::isFinite(targetPoint)) {
                msg_targetPoints.targetPoint[0] = targetPoint.x;
                msg_targetPoints.targetPoint[1] = targetPoint.y;
                msg_targetPoints.targetPoint[2] = targetPoint.z;
                msg_cursor.linear.x = targetPoint.x;
                msg_cursor.linear.y = targetPoint.y;
                msg_cursor.linear.z = targetPoint.z;

            }
            if(pcl::isFinite(trackingPoint)) {
                msg_targetPoints.trackingPoint[0] = trackingPoint.x;
                msg_targetPoints.trackingPoint[1] = trackingPoint.y;
                msg_targetPoints.trackingPoint[2] = trackingPoint.z;
            }

            chatter_pub3.publish(msg_targetPoints);
            chatter_pub4.publish(msg_cursor);
        }

        msg_selectedObject.topSphere.position.x = topSphereWam[0];
        msg_selectedObject.topSphere.position.y = topSphereWam[1];
        msg_selectedObject.topSphere.position.z = topSphereWam[2];
        msg_selectedObject.topSphere.orientation.x = 0;
        msg_selectedObject.topSphere.orientation.y = 1;
        msg_selectedObject.topSphere.orientation.z = 0;
        msg_selectedObject.topSphere.orientation.w = 0;

        msg_selectedObject.centroid.x = centroidSphere[0];
        msg_selectedObject.centroid.y = centroidSphere[1];
        msg_selectedObject.centroid.z = centroidSphere[2];

        for(int i = 0;i<circleSphereDivision;i++) {
            msg_selectedObject.circularSphere[i].position.x = circularSphere[i].point[0];
            msg_selectedObject.circularSphere[i].position.y = circularSphere[i].point[1];
            msg_selectedObject.circularSphere[i].position.z = circularSphere[i].point[2];
            msg_selectedObject.circularSphere[i].orientation.x = circularSphere[i].orientation.x();
            msg_selectedObject.circularSphere[i].orientation.y = circularSphere[i].orientation.y();
            msg_selectedObject.circularSphere[i].orientation.z = circularSphere[i].orientation.z();
            msg_selectedObject.circularSphere[i].orientation.w = circularSphere[i].orientation.w();
        }

        chatter_pub6.publish(msg_selectedObject);
        ros::spinOnce();
        loop_rate.sleep();
    }
}
